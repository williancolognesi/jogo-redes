package jogo;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import models.Jogador;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.state.*;

import socket.ClienteChat;
import socket.ClienteChatAtualiza;

public class Play extends BasicGameState {
	
	private ClienteChat chat;
	private ImageAnalyzer iA;
	private Map map;
	private Circle objetoColisao;

	public static Jogador playerLocal;
	private float speed = 0.2f;
	public static List<Jogador> JOGADORES = new ArrayList<Jogador>();
	private boolean finalizou = false;
	///Users/Willian/Documents/Faculdade/Laboratorio de Computação/workspace2/Jogo_mario/libs/slick
	public Play(int xSize, int ySize) {
		chat = new ClienteChat();
		chat.start();
		this.playerLocal = new Jogador();
		
		String nome = null;
		while((nome == null)){
			nome = JOptionPane.showInputDialog("Digite seu nickname!");
			if(nome == null){
				JOptionPane.showMessageDialog(null, "Digite um nickname válido!");
			}else if(nome.isEmpty()){
				JOptionPane.showMessageDialog(null, "Digite um nickname válido!");
				nome = null;
			}else{
				this.playerLocal.setNome(nome);
				
				JSONObject obj = new JSONObject();
				obj.put("codigo", 13);
				obj.put("nickname", this.playerLocal.getNome());
				
				chat.MSG_ENVIAR = obj;	
			}
		}
	}

	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
		iA = new ImageAnalyzer();
		iA.init();
		map = new Map(iA, 1280, 768);
		
		playerLocal.setBoneco(new Image("res/minhoca_dir.png"));
		objetoColisao = new Circle(playerLocal.getCord_x(), playerLocal.getCord_y(), 10);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		map.getMapImage().draw(0, 0);
		
		playerLocal.getBoneco().draw(playerLocal.getCord_x(), playerLocal.getCord_y());
		gc.getGraphics().drawString(playerLocal.getNome(), playerLocal.getCord_x(), playerLocal.getCord_y()); // nick do jogador
		
		for(Jogador jogador : JOGADORES){
			if(jogador.getLado().equals("D")){
				jogador.setBoneco(new Image("res/minhoca_dir.png"));	
			}else{
				jogador.setBoneco(new Image("res/minhoca_esq.png"));
			}
			
			gc.getGraphics().drawString(jogador.getNome(), jogador.getCord_x(), jogador.getCord_y()); // nick do jogador
			jogador.getBoneco().draw(jogador.getCord_x(), jogador.getCord_y());
		}
		
		atualizaChat(gc);
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		Input input = gc.getInput();
		
		float oldXPos = this.playerLocal.getCord_x();
		float oldYPos = this.playerLocal.getCord_y();
		
		if (input.isKeyDown(Input.KEY_W)) {
			this.playerLocal.setCord_y(this.playerLocal.getCord_y() - (delta*speed));
        }
        if (input.isKeyDown(Input.KEY_S)) {
        	this.playerLocal.setCord_y(this.playerLocal.getCord_y() + (delta*speed));
        }
        if (input.isKeyDown(Input.KEY_A)) {
            this.playerLocal.setCord_x(this.playerLocal.getCord_x() - (delta*speed));
            this.playerLocal.setLado("E");
            playerLocal.setBoneco(new Image("res/minhoca_esq.png"));
            
        }
        if (input.isKeyDown(Input.KEY_D)) {
            this.playerLocal.setCord_x(this.playerLocal.getCord_x() + (delta*speed));
            this.playerLocal.setLado("D");
            playerLocal.setBoneco(new Image("res/minhoca_dir.png"));
        }
        
		if(input.isKeyPressed(Input.KEY_F)){
			String msg = JOptionPane.showInputDialog("Digite a mensagem a ser enviada!");
			if(msg != null){
				if(msg.length() > 18){
					JOptionPane.showMessageDialog(null, "A mensagem deve ter até 18 Carateres!");
				}else{
					JSONObject obj = new JSONObject();
					obj.put("codigo", 1);
					obj.put("mensagem", msg);
					obj.put("nickname", this.playerLocal.getNome());
					
					chat.MSG_ENVIAR = obj;	
				}
			}
			
		}
		
        objetoColisao.setLocation(this.playerLocal.getCord_x() + 3, this.playerLocal.getCord_y() + 13);		
		if(map.intersects(objetoColisao)){
			this.playerLocal.setCord_x(oldXPos);
			this.playerLocal.setCord_y(oldYPos);
			
			objetoColisao.setLocation(oldXPos + 3, oldYPos + 13);
		}
		
		int coord_x = (int)this.playerLocal.getCord_x();
		int coord_y = (int)this.playerLocal.getCord_y();
		if( (coord_x >= 1213 && coord_x <= 1224)
				&&
			coord_y >= 610){
			JSONObject obj = new JSONObject();
			obj.put("codigo", 99);
			obj.put("mensagem", "Vencedor!!");
			obj.put("nickname", this.playerLocal.getNome());
			if(!finalizou){
				chat.MSG_ENVIAR = obj;	
			}
			finalizou = true;
			
		}
		JSONObject cordJson = new JSONObject();
		cordJson.put("codigo", 10);
		cordJson.put("lado", this.playerLocal.getLado());
		cordJson.put("cord_x", this.playerLocal.getCord_x());
		cordJson.put("cord_y", this.playerLocal.getCord_y());
		cordJson.put("nickname", this.playerLocal.getNome());
		chat.POSICAO_ENVIAR = cordJson;
		
	}

	private void atualizaChat(GameContainer gc){
		JSONArray chat = ClienteChatAtualiza.CHAT_RECEBIDO;
		
		if(chat != null){
			int inicio = 30;
			for(int i = 0; i < chat.length(); i++){
				JSONObject obj = chat.getJSONObject(i);
				if(obj.getInt("codigo") == 99){
					gc.getGraphics().setColor(Color.red);
					gc.getGraphics().drawString(obj.getString("nickname") + " Chegou!", 1050, (inicio+17)); // nick do jogador
				}else{
					gc.getGraphics().setColor(Color.white);
					gc.getGraphics().drawString(obj.getString("nickname") + ": " + obj.getString("mensagem"), 1050, (inicio+17)); // nick do jogador	
				}
				
				inicio += 17;
			}
		}
		
	}
	
	public int getID() {
		return 0;
	}
}