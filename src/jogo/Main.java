package jogo;
import java.awt.SplashScreen;
import java.util.ArrayList;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.*;

public class Main extends StateBasedGame{
   
   public static final String gamename = "Jogo Unifil";
   public static final int play = 0;
   public static final int xSize = 1280; //40 tiles
   public static final int ySize = 768;//24 tiles
   
   
   public Main(String gamename){
      super(gamename);
      
      this.addState(new Play(xSize, ySize));
   }
   
   public void initStatesList(GameContainer gc) throws SlickException{
      this.getState(play).init(gc, this);
      this.enterState(play);
   }
   
   public static void main(String[] args) {
      AppGameContainer appgc;
      try{
         appgc = new AppGameContainer(new Main(gamename));
         appgc.setDisplayMode(xSize, ySize, false);
         appgc.setTargetFrameRate(60);
         appgc.start();
      }catch(SlickException e){
         e.printStackTrace();
      }
   }
}