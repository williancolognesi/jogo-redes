package jogo;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageAnalyzer {

	BufferedImage map;
	int[][] tiles;
	String path = "src/res/map.png"; // pathen til bildet. res er i prosjektmappa

	public void init() {
		openFile();
		tiles = new int[map.getWidth()][map.getHeight()];
		clear();
		read();
		//print();
	}
	
	public int getTile(int x, int y){
		return tiles[x][y];
	}

	// Opprettar biletet (BufferedImage) map fr� path
	private void openFile() {
		try {
			map = ImageIO.read(new File(path));
		} catch (IOException e) {
			System.err.println("File not found");
		}
	}

	// Set alle indeksane i tiles til 0
	private void clear() {
		for (int i = 0; i < map.getHeight(); i++) {
			for (int j = 0; j < map.getWidth(); j++) {
				tiles[j][i] = 0; 
			}
		}
	}

	// Les RGB verdien til kvar piksel i .png fila og set ein verdi til indeksa i tiles arrayen
	private void read() {
		for (int i = 0; i < map.getHeight(); i++) {
			for (int j = 0; j < map.getWidth(); j++) {
				if (map.getRGB(j, i) == -16777216) tiles[j][i] = 1; // #000000 / Svart --> golv
				if (map.getRGB(j, i) == -65426) tiles[j][i] = 2; // #FF006E / Rosa --> stigar
				
			}
		}
	}

	private void print() {
		for (int i = 0; i < map.getHeight(); i++) {
			for (int j = 0; j < map.getWidth(); j++) {
				if (tiles[j][i] == 0) System.out.print(". ");
				if (tiles[j][i] == 1) System.out.print("� ");
				if (tiles[j][i] == 2) System.out.print("H ");
			}
			System.out.println();
		}
		//System.out.println(colourPicker(5,5));
	}
	
	// Returnerer RGB_INT verdien til piksel x, y
	private int colourPicker(int x, int y){
		int colour = map.getRGB(x, y);
		return colour;
	}
}
