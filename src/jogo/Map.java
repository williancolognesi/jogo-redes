package jogo;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;


public class Map {
	Image map;
	SpriteSheet sheet;
	Graphics gI;
	ArrayList<Shape> geoList = new ArrayList<Shape>();
	
	public Map(ImageAnalyzer aI, int xSize, int ySize){
		Rectangle r; 
		try {
			map = new Image(xSize, ySize);
			sheet = new SpriteSheet("res/tiles.png", 32, 32);
			gI = map.getGraphics();
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i=0; i<xSize/32; i++){
			for(int j=0; j<ySize/32; j++){
				if(aI.getTile(i,j) == 0){
					gI.drawImage(sheet.getSubImage(0, 0), i*32, j*32);
				}
				if(aI.getTile(i,j) == 1){
					gI.drawImage(sheet.getSubImage(0, 2), i*32, j*32);
					r = new Rectangle(i*32, j*32, 32, 32);
					geoList.add(r);
				}
			}
		}
		gI.flush();
		
		
	}
	
	public void renderGeo(Graphics g){
		for(int i=0; i<geoList.size(); i++){
			g.draw(geoList.get(i));
		}
		
	}
	
	public boolean intersects(Shape s){
		boolean b = false;
		
		for(int i=0; i<geoList.size(); i++){
			if(s.intersects(geoList.get(i))){
				b = true;
			}
		}
		
		return b;
	}
	
	public Image getMapImage(){
		return map;
	}
}
