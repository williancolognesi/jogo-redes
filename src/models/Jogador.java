package models;

import org.newdawn.slick.Image;

public class Jogador {

	private String nome;
	private Image boneco;
	private float cord_x;
	private float cord_y;
	private String lado = "D";
	
	public Jogador(){
		this.cord_x = 150;
		this.cord_y = 150;
	}
	
	public void setLado(String lado){
		this.lado = lado;
	}
	
	public String getLado(){
		return this.lado;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Image getBoneco() {
		return boneco;
	}
	public void setBoneco(Image boneco) {
		this.boneco = boneco;
	}
	public float getCord_x() {
		return cord_x;
	}
	public void setCord_x(float cord_x) {
		this.cord_x = cord_x;
	}
	public float getCord_y() {
		return cord_y;
	}
	public void setCord_y(float cor_y) {
		this.cord_y = cor_y;
	}
	
}
