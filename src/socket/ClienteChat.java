package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.JOptionPane;

import org.json.JSONObject;

import jogo.Play;

/**
 *
 * @author willian.colognesi
 */
public class ClienteChat extends Thread{
    
    private static int PORTA = 65000;
    private static String SERVER = "localhost";
    public JSONObject MSG_ENVIAR = null;
    public JSONObject POSICAO_ENVIAR = null;
    
    @Override
    public void run(){
        Socket s = null;
        PrintStream ps = null;
        System.out.println("Iniciando Thread do chat!");
        try {
            s = new Socket(SERVER, PORTA);
            ps = new PrintStream(s.getOutputStream());
//            BufferedReader entrada = new BufferedReader(new InputStreamReader(s.getInputStream()));
            Thread threadAtualizaChat = new ClienteChatAtualiza(s);
            threadAtualizaChat.start();
            
            while(true){
            	if(MSG_ENVIAR != null){
                	ps.println(MSG_ENVIAR);
                	MSG_ENVIAR = null;
                }
            	
            	if(POSICAO_ENVIAR != null){
            		ps.println(POSICAO_ENVIAR);
            		POSICAO_ENVIAR = null;
            	}
            	try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
            }
            
        } catch (IOException e) {
            System.out.println("Algum problema ocorreu ao criar ou enviar dados pelo Socket.");
            e.printStackTrace();
        } finally {
            try {
                s.close();
            } catch (Exception e) {
                System.out.println("Algum problema ocorreu ao finalizar o socket");
            }
        }
    }
    
}