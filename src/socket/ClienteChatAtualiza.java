package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JOptionPane;

import models.Jogador;

import org.json.JSONArray;
import org.json.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import jogo.Play;

public class ClienteChatAtualiza extends Thread{
	
	private static Socket socket = null;
	public static JSONArray CHAT_RECEBIDO;
	
	public  ClienteChatAtualiza(Socket s) {
		this.socket = s;
	}
	
	@Override
	public void run(){
		System.out.println("Iniciando Thread de Comunicação do jogo");
		try {
			BufferedReader entrada = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			
			String msg_srv = "";
			while(true){
				msg_srv = entrada.readLine();
				if(msg_srv != null){
					JSONArray arrayRecebido = new JSONArray(msg_srv);
					if(arrayRecebido.length() > 0){
						JSONObject obj = arrayRecebido.getJSONObject(0);
						
						switch(obj.getInt("codigo")){
						
							case 1:
								ClienteChatAtualiza.CHAT_RECEBIDO = arrayRecebido;
								System.out.println(ClienteChatAtualiza.CHAT_RECEBIDO);
								break;
						
							case 6:
								for(int i = 0; i < Play.JOGADORES.size(); i++){
									if(obj.getString("nickname").equals(Play.JOGADORES.get(i).getNome())){
										Play.JOGADORES.remove(i);
										if(ClienteChatAtualiza.CHAT_RECEBIDO == null){
											ClienteChatAtualiza.CHAT_RECEBIDO = new JSONArray();
										}
										ClienteChatAtualiza.CHAT_RECEBIDO.put(obj);
										break;
									}
								}
								break;
								
							case 10:
								for(int i =0; i < arrayRecebido.length(); i++){
									JSONObject objPlayer = arrayRecebido.getJSONObject(i);
									boolean encontrou = false;
									
									Jogador jog = new Jogador();
									jog.setNome(objPlayer.getString("nickname"));
									jog.setCord_x(Float.parseFloat(String.valueOf(objPlayer.getDouble("cord_x"))));
									jog.setCord_y(Float.parseFloat(String.valueOf(objPlayer.getDouble("cord_y"))));
									jog.setLado(objPlayer.getString("lado"));
//									jog.setBoneco(new Image("res/minhoca_dir.png"));
									
									if(!jog.getNome().equals(Play.playerLocal.getNome())){
										for(int j =0; j < Play.JOGADORES.size(); j++){
											if(Play.JOGADORES.get(j).getNome().equals(jog.getNome())){
												Play.JOGADORES.set(j, jog);
												encontrou = true;
											}
										}
										if(!encontrou){
											Play.JOGADORES.add(jog);
										}
									}
									
								}
								
								
								break;
						}
					}
				}
				
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			
		} catch (IOException e) {
			System.out.println("erro ao receber mensagem do servidor " + e);
			e.printStackTrace();
		}
	}
	
}
